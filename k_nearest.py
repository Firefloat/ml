# libraries
import numpy as np
import scipy as sp
from tensorflow import keras
from matplotlib import pyplot as plt

# print options
np.set_printoptions(linewidth=150, precision=2)

# retrieve train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# Model
class KNearestNeighbor(object):
    train_data = None
    train_labels = None
    nearest_neighbors = None
    k = 1

    def __calc_euclidean_distance(self, value_vector):
        dist = np.linalg.norm(self.train_data - value_vector, axis=1)
        min_index = np.argsort(dist)[0:self.k]
        self.nearest_neighbors = self.train_labels[min_index]
        
    def train(self, train_data, train_labels):
        self.train_data = train_data
        self.train_labels = train_labels

        # sort both arrays ascending
        sorted_i = np.argsort(train_labels)
        self.train_data = self.train_data[sorted_i].reshape(self.train_data.shape[0], self.train_data.shape[1]**2)
        self.train_labels = self.train_labels[sorted_i]

    def fit(self, value_vector, k=1, distance="euclid"):
        self.nearest_neighbors = np.zeros(k)
        self.k = k
        value_vector = value_vector.reshape(value_vector.shape[0]**2)
        if distance == "euclid":
            self.__calc_euclidean_distance(value_vector)
        else:
            print("Error")
        counts = np.bincount(self.nearest_neighbors.astype(int))
        return np.argmax(counts)

knn = KNearestNeighbor()
knn.train(x_train, y_train)
predicted = np.zeros(50)

for index in range(50):
    predicted[index] = knn.fit(x_test[index], k=1)
    if not (index % 1000) and index != 0:
        print(f'{index} done!')

matches = np.where(predicted == y_test[0:50])[0].shape[0]
print(f'{matches} matches from {50}, that is a train error of {1-(matches/50)}')
